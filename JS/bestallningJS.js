$(function(){
    getBestalning();
    $("#update").hide();
});
getBestalning = async function () {
    $.ajax({
      url: "http://localhost:9090/api/v1/myorders",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
        "id" : sessionStorage.getItem("userId")
      },
      success: function (orderArray) {
        buildTable(orderArray);
      },
    });
};

buildTable = function(orders){
    orderTable = $("#bestalningTbody");
    orderTable.html("");

    standardForm(orderTable);
    for (var i = 0; i < orders.length; i++) {
    tableRow = $(`<tr class = 'orderTableRow'> </tr>`);
    orderTable.append(tableRow);

    tableCell = $(`<td class='orderTableCell' id='oerderId'> </td>`);
    tableCell.html(`<button class='update btn funktion' id="${i}">ändra</button>`);
    tableRow.append(tableCell);

    tableCell = $(`<td class='orderTableCell' id='startDate'> </td>`);
    tableCell.html(orders[i].startDate);
    tableRow.append(tableCell);

    tableCell = $(`<td class='orderTableCell' id='returnDate'> </td>`);
    tableCell.html(orders[i].returnDate);
    tableRow.append(tableCell);

    tableCell = $(`<td class='orderTableCell' id='customerName'> </td>`);
    tableCell.html(orders[i].customer.name);
    tableRow.append(tableCell);

    tableCell = $(`<td class='orderTableCell' id='carName'> </td>`);
    tableCell.html(orders[i].car.name);
    tableRow.append(tableCell);

    tableCell = $(`<td class='orderTableCell' id='carModell'> </td>`);
    tableCell.html(orders[i].car.modell);
    tableRow.append(tableCell);
    
    if(orders[i].canceled == false){
      tableCell = $(`<td class='orderTableCell' id='oerderId'> </td>`);
      tableCell.html(`<button class='cancel btn negative' id="${i}">avbryt</button>`);
      tableRow.append(tableCell);
    } else{
      tableCell = $(`<td class='orderTableCell' id='oerderId'> </td>`);
      tableCell.html(`<button class='btn' id="canceld">avslutad</button>`);
      tableRow.append(tableCell);
    }
  }
    
  $( ".update" ).on("click", function(){
    uppdateForm(orders[$(this)[0].id])
  });

  $( ".cancel" ).on("click", function(){
    cancelorder(orders[$(this)[0].id])
  });


};

uppdateForm = function(order){
  $("#update").show();
  oldTable = $("#bestalningTbody");
  oldTable.html("");

  orderTable = $("#uppBestalningTbody");
    orderTable.html("");

  tableRow = $(`<tr class = 'updateTableRow'> </tr>`)
  orderTable.append(tableRow);
  


  tableCell = $(`<td> </td>`);
  tableCell = $(`<label>start</label>
  <input type="date" id="startDateUppdate">`);
  tableRow.append(tableCell);

  tableCell = $(`<td> </td>`);
  tableCell = $(`<label>slut</label>
  <input type="date" id="endDateUppdate">`);
  tableRow.append(tableCell);

  tableCell = $(`<td> </td>`);
  tableCell = $(`<label>bil</label>
  <input type="text" id="updateCarId">`);
  tableRow.append(tableCell);

  tableCell = $(`<td> </td>`);
  tableCell = $(`<label>customer</label>
  <input type="text" id="updateCustomerId">`);
  tableRow.append(tableCell);

  $("#startDateUppdate").val(order.startDate);
  $("#endDateUppdate").val(order.returnDate);
  $("#updateCarId").val(order.car.id);
  $("#updateCustomerId").val(order.customer.id);

  $("#update").on("click", function(){
    var startDate  = $("#startDateUppdate").val();
    var returnDate = $("#endDateUppdate").val();
    var orderId = order.id;
    var carId = $("#updateCarId").val();
    var customerId = $("#updateCustomerId").val();
    
    customeritem = {}
    customeritem["id"] = customerId;

    caritem = {}
    caritem["id"] = carId;

    item = {}
    item["startDate"] = startDate;
    item["returnDate"] = returnDate;
    item["id"] = orderId;
    item["customer"] = customeritem;
    item["car"] = caritem;
    console.log(item);
    uppdateSend(item);
  });
}

cancelorder = function(order){
  customeritem = {}
  customeritem["id"] = order.customer.id;

  caritem = {}
  caritem["id"] = order.car.id;

  item = {}
  item["startDate"] = order.startDate;
  item["returnDate"] = order.returnDate;
  item["id"] = order.id;
  item["canceled"] = true;
  item["customer"] = customeritem;
  item["car"] = caritem;
  console.log(item);

  uppdateSend(item);
}

standardForm = function(orderTable){
  
  tableRow = $(`<tr class = 'orderTableRow'> </tr>`);
  orderTable.append(tableRow);

  tableCell = $(`<td class='orderTableCell' id='orderId'> </td>`);
  tableCell.html();
  tableRow.append(tableCell);

  tableCell = $(`<td class='orderTableCell' id='startDate'> </td>`);
  tableCell.html("start datum");
  tableRow.append(tableCell);

  tableCell = $(`<td class='orderTableCell' id='returnDate'> </td>`);
  tableCell.html("retur datum");
  tableRow.append(tableCell);

  tableCell = $(`<td class='orderTableCell' id='Namn'> </td>`);
  tableCell.html("kund namn");
  tableRow.append(tableCell);

  tableCell = $(`<td class='orderTableCell' id='carName'> </td>`);
  tableCell.html("bil märke");
  tableRow.append(tableCell);

  tableCell = $(`<td class='orderTableCell' id='carModell'> </td>`);
  tableCell.html("bil modell");
  tableRow.append(tableCell);

  tableCell = $(`<td class='orderTableCell' id='canceldButton'> </td>`);
  tableCell.html("");
  tableRow.append(tableCell);
}
uppdateOrder = function(order){
  uppdateSend(order)
}

uppdateSend = async function (order) {
  $.ajax({
    url: "http://localhost:9090/api/v1/updateorder",
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    data: JSON.stringify(order),
    success: function () {
      $(".display").load("/html/bestallning.html");
    },
  });
};