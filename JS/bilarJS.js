$(function () {
  $("#LedigCheck").on("click",function(){
    if(getStartDate() === null){
      alert("vänligen sätt ett start datum")
    }
    else if(getendDate() === null){
      alert("vänligen sätt ett slut datum")
    } else if(($("#startDate").val() > ($("#returnDate").val()))){
      alert("vänligen sätt slut datumet senare än start datumet")
    } else{
      getCars($("#returnDate").val(), $("#returnDate").val());
    }
  });
});

function getStartDate(){
  let date = new Date($("#startDate").val());

  if(!Date.parse(date)){
    return null
  } else {

  let day = date.getDate();
  if(day < 10 && day > 0){
    day = "0" + day
  }
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  var start = year + "-" + month + "-" + day;

  return start;
  }

}
function getendDate(){
  let date = new Date($("#returnDate").val());

  if(!Date.parse(date)){
    return null
  }
  else {

  let day = date.getDate();
  if(day < 10 && day > 0){
    day = "0" + day
  }
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  var start = year + "-" + month + "-" + day;

  return start;
} 
}

getCars = async function (startDay, returnDay) {
  $.ajax({
    url: "http://localhost:9090/api/v1/cars",
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
      "startDay": startDay,
      "returnDay" : returnDay,
    },
    success: function (carArray) {
      buildTable(carArray);
    },
  });
};

buildTable = function (cars) {
  carTable = $("#carTableBody");
  carTable.html("");
  standardForm();
  for (var i = 0; i < cars.length; i++) {
    tableRow = $(`<tr class = 'carTableRow'> </tr>`);
    carTable.append(tableRow);

    tableCell = $(`<td class='carTableCell' id='carId'> </td>`);
    tableCell.html(`<button class='bokCar btn positiv' id="${cars[i].id}">boka</button>`);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='model'> </td>`);
    tableCell.html(cars[i].modell);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='name'> </td>`);
    tableCell.html(cars[i].name);
    tableRow.append(tableCell);

    tableCell = $(`<td class='carTableCell' id='PricePerDay'> </td>`);
    tableCell.html(cars[i].pricePerDay);
    tableRow.append(tableCell);
   
  }

  $( ".bokCar" ).on("click", function(){
    sendOrder($(this)[0].id);
  });

};

standardForm = function(){
  carTable = $("#carTableBody");

  tableRow = $(`<tr class = 'carTableRow'> </tr>`);
  carTable.append(tableRow);

  tableCell = $(`<td class='carTableCell' id='carId'> </td>`);
  tableCell.html();
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCell' id='model'> </td>`);
  tableCell.html("model");
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCell' id='name'> </td>`);
  tableCell.html("name");
  tableRow.append(tableCell);

  tableCell = $(`<td class='carTableCell' id='PricePerDay'> </td>`);
  tableCell.html("Price per Day");
  tableRow.append(tableCell);
}
sendOrder = async function(id){
  const order = creatOrder(id);
  console.log(order);
  $.ajax({
    url: "http://localhost:9090/api/v1/ordercar",
    method: "POST",
    
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Authorization": "Basic " + btoa(sessionStorage.getItem("userName") + ":" + sessionStorage.getItem("userPassword")),
    },
    data: JSON.stringify(order),
      success : function(){
        alert("bil bokad");
        $(".display").load("/html/bilar.html");
      }
      
  });
}
function creatOrder(id){

var startDate = getStartDate();
var returnDate = getendDate();
var customerid = sessionStorage.getItem("userId");
var carid = id

customeritem = {}
customeritem["id"] = customerid

caritem = {}
caritem["id"] = carid

item = {}
item["startDate"] = startDate;
item["returnDate"] = returnDate;
item["customer"] = customeritem;
item["car"] = caritem;

return item;
}
